import { Action } from '../../types/store';
import * as TYPES from './constants';

export type SearchPhotosState = Readonly<{
  searching: boolean,
  phrase: string,
  totalResults: number,
  totalPages:  number,
  page: number,
  photos: JSON[] | null,
  error: string,
}>;

export const initialState: SearchPhotosState = {
  searching: false,
  phrase: "",
  totalResults: 0,
  totalPages: 1,
  page: 1,
  photos: null,
  error: "",
};

export default function searchPhotosReducer(
  state = initialState,
  action: Action,
): SearchPhotosState {
  const { type = '', payload = null } = action;

  switch (type) {
    case TYPES.GET_PHOTOS_SEARCHING: {
      return { ...state, searching: true, error: "" };
    }

    case TYPES.GET_PHOTOS_ERROR: {
      return { ...state, searching: false, error: payload };
    }

    case TYPES.GET_PHOTOS_RESULTS: {
      return {
        ...state,
        searching: false,
        totalResults: payload.total,
        totalPages: payload.total_pages,
        page: 1,
        photos: payload.results,
      };
    }

    case TYPES.GET_PHOTOS_NEXT_PAGE_RESULTS: {
      return {
        ...state,
        searching: false,
        photos: [...state.photos, ...payload.results],
      };
    }

    case TYPES.UPDATE_PAGE: {
      return { ...state, page: payload };
    }

    case TYPES.UPDATE_PHRASE: {
      return { ...state, phrase: payload };
    }

    default: {
      return state;
    }
  }
}
