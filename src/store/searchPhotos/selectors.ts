import { SearchPhotosState } from './reducer';

const getState = (state: any): SearchPhotosState => state.searchPhotosStore || {};

export const getPhotos = (state: any) => {
  const photosState = getState(state);
  return photosState.photos;
};

export const getPhrase = (state: any) => {
  const photosState = getState(state);
  return photosState.phrase;
};

export const getLoading = (state: any) => {
  const photosState = getState(state);
  return photosState.searching;
};

export const getError = (state: any) => {
  const photosState = getState(state);
  return photosState.error;
};

export const getMaxPage = (state: any) => {
  const photosState = getState(state);
  return photosState.totalPages;
};

export const getPage = (state: any) => {
  const photosState = getState(state);
  return photosState.page;
};
