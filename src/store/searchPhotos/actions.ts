import { create } from '..';
import * as TYPES from './constants';
import { Responce } from '../../types/searchPhotosResponse';

export const searchingPhotos = () => create(TYPES.GET_PHOTOS_SEARCHING);
export const searchingError = (message: string) => create(TYPES.GET_PHOTOS_ERROR, message);
export const searchingResults = (response: Responce) => create(TYPES.GET_PHOTOS_RESULTS, response);
export const searchingNextPageResults = (response: Responce) => create(TYPES.GET_PHOTOS_NEXT_PAGE_RESULTS, response);
export const updatePage = (page: number) => create(TYPES.UPDATE_PAGE, page);
export const updatePhrase = (phrase: string) => create(TYPES.UPDATE_PHRASE, phrase);
