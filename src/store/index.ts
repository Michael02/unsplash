import createStore from './create';
import searchPhotosStore, { initialState as searchPhotosInitialState } from './searchPhotos/reducer';

const reducers = {
  searchPhotosStore,
};

const initialState = {
  searchPhotosStore: searchPhotosInitialState,
};

const store = createStore(reducers, initialState);

export const create = (type: string, payload?: any) => ({ type, payload });
export type Store = typeof store;
export default store;
