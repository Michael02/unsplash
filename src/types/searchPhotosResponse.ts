export type Responce = {
  total: number,
  total_pages: number,
  results: JSON[] | null,
};
