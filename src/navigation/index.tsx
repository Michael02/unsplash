import * as React from "react";
import {
  HashRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import MainPage from '../pages/MainPage';
import PhotosPage from '../pages/Photos';

const MainNavigation = () => {
  return (
   <Router>
     <Switch>
        <Route exact path="/" component={ MainPage } />
        <Route path="/photos" component={ PhotosPage } />
     </Switch>
   </Router> 
  );
};

export default MainNavigation;
