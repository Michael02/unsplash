import { ACCESS_KEY, SECRET_KEY } from '../.env';
const Unsplash = require('unsplash-js').default;

const unsplashApiService = new Unsplash({
  accessKey: ACCESS_KEY,
  secret: SECRET_KEY,
});

const unsplashAutocompleteService = new Unsplash({
  apiUrl: 'https://unsplash.com/nautocomplete',
  accessKey: ACCESS_KEY,
  secret: SECRET_KEY,
});


class ApiServiceClass {
  async searchPhotos(phrase: string, page: number = 1) {
    const res = await unsplashApiService.search.photos(phrase, page, 25);
    const jsonRes = this.handleResponse(res);
    return jsonRes;
  }

  async getPhoto(id: string) {
    const res = await unsplashApiService.photos.getPhoto(id);
    const jsonRes = this.handleResponse(res);
    return jsonRes;
  }

  // wordComplete = () => {
  //   unsplashAutocompleteService.request({
  //     url: '/physics',
  //     method: 'GET'
  //   })
  //     .then((res: any) => { console.log(res); });
  // }

  async handleResponse(response: any) {
    try {
      if (!response.ok) {
        const error = await response.json();
        throw new Error(error.message);
      }
      
      const json = await response.json();
      return json;
    } catch(error) {
      throw new Error(error);
    }
  } 
}

const ApiService = new ApiServiceClass();
export default ApiService;
