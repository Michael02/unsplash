import * as React from 'react';
import { connect } from 'react-redux';
import { RouteChildrenProps } from 'react-router-dom';
const Masonry = require("react-responsive-masonry").default;

import SearchBar from '../../components/SearchBar';
import Photo from '../../components/PhotoContainer';
import ModalPhoto from '../../components/ModalPhoto';
import Loader from '../../components/Loader';
import ApiService from '../../services/api.service';
import {
  getPhotos,
  getPhrase,
  getPage,
  getMaxPage,
  getLoading,
  updatePage,
  searchingPhotos,
  searchingNextPageResults,
} from '../../store/searchPhotos';
import './Photos.scss';

const select = (state: any) => ({
  photos: getPhotos(state),
  phrase: getPhrase(state),
  page: getPage(state),
  maxPage: getMaxPage(state),
  loading: getLoading(state),
});
const mapActions = {
  updatePage,
  searchingPhotos,
  searchingNextPageResults,
};

type MappedProps = ReturnType<typeof select>;
type MappedActions = typeof mapActions;

type Props = {} & MappedActions & MappedProps & RouteChildrenProps;
type State = {
  showModal: boolean,
  modalData: any,
};

class PhotosPage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      showModal: false,
      modalData: null
    };
  }

  componentDidMount() {
    if (!this.props.photos && !this.props.phrase) this.props.history.push('/');
    document.addEventListener('scroll', this.scrollTracking);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.scrollTracking);
  }

  scrollTracking = (e: WheelEvent) => {
    // check if at the bottom of page
    const scrollDelta = document.body.scrollHeight - window.innerHeight - window.scrollY;
    const { loading, page, maxPage } = this.props;
    if (scrollDelta <= 200 && !loading && page < maxPage) {
      this.getNextPage(page + 1);
    }
  }

  getNextPage = (page: number) => {
    this.props.searchingPhotos();
    ApiService.searchPhotos(this.props.phrase, page)
      .then((res) => {
        this.props.searchingNextPageResults(res);
        this.props.updatePage(page);
      });
  }

  dismisModal = () => {
    this.setState({
      showModal: false,
      modalData: null,
    });
  }

  handleImageClick = (data: any) => {
    ApiService.getPhoto(data.id)
      .then((res) => {
        this.setState({
          showModal: true,
          modalData: res,
        });
      });
  }

  renderPhotos = () => {
    const { photos } = this.props;
    if (!photos) return null;
    return photos.map((item: any) => <Photo key={item.id} data={item} onClick={this.handleImageClick}/>);
  }

  renderModal = () => {
    const { showModal, modalData } = this.state;
    if (showModal && modalData) return <ModalPhoto data={modalData} onDismis={ this.dismisModal }/>;
    return null;
  }

  renderLoader = () => {
    const { loading } = this.props;
    if (!loading) return null;
    return (
      <div className="row">
        <div className="col-12 mt-2 d-flex justify-content-center">
          <Loader/>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="d-flex justify-content-center">
        <div className="main-container">
          <header className="row">
            <div className="col-8">
              <SearchBar history={ this.props.history }/>
            </div>
          </header>
          <hr/>
          <section className="row">
            <div className="col-12">
              <Masonry>
                { this.renderPhotos() }
              </Masonry>
            </div>
          </section>
          { this.renderLoader() }
        </div>
        { this.renderModal() }
      </div>
    );
  }
}

export default connect<MappedProps, MappedActions>(select, mapActions)(PhotosPage);
