import * as React from 'react';
import { RouteChildrenProps } from 'react-router-dom';
import { connect } from 'react-redux';

import SearchBar from '../../components/SearchBar';
import Loader from '../../components/Loader';
import { getLoading } from '../../store/searchPhotos';
import './MainPage.scss';

const select = (state: any) => ({
  loading: getLoading(state),
});
const mapActions = {};

type MappedProps = ReturnType<typeof select>;
type MappedActions = typeof mapActions;

type Props = {} & MappedActions & MappedProps & RouteChildrenProps;
type State = {};

class MainPage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  renderLoader = () => {
    const { loading } = this.props;
    if (!loading) return null;
    return (
      <div className="row">
        <div className="col-12 mt-2 d-flex justify-content-center">
          <Loader/>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="d-flex justify-content-center">
        <div className="main-container margin-at-top">
          <h2>Unsplash</h2>
          <p>
            The internet’s source of freely-usable images.<br/>
            Powered by creators everywhere.
          </p>
          <SearchBar history={ this.props.history }/>
          { this.renderLoader() }
        </div>
      </div>
    );
  }
}

export default connect<MappedProps, MappedActions>(select, mapActions)(MainPage);
