import * as React from 'react';
import { GeoAlt } from 'react-bootstrap-icons';

import './ModalPhoto.scss';

type Props = {
  data: any,
  onDismis: Function,
};

const Photo: React.SFC<Props> = (props) => {
  const {
    data,
    onDismis,
  } = props;
  const location = data.location.title;
  return (
    <div className="d-flex modal-container">
      <div className="bg-shadow" onClick={() => onDismis()}/>
      <div className="modal-body">
        <div className="header d-flex justify-content-between mb-2">
          <div className="d-flex align-items-center">
            <div className="user-image">
              <img src={ data.user.profile_image.medium } />
            </div>
            <div className="user-data d-flex flex-column">
              <p className="user-name">{ data.user.name }</p>
              <p className="user-handle">@{ data.user.username }</p>
            </div>
          </div>
          <button
            type="button"
            className="close"
            aria-label="Close"
            onClick={ () => onDismis() }
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="main-image d-flex justify-content-center">
          <a href={ data.urls.full } target="_blank">
            <img src={ data.urls.full } />
          </a>
        </div>
        <div className="footer d-flex align-items-center mt-2">
          { location ? (
            <div className="mr-3">
              <p className="font-weight-bold"><GeoAlt color="#000" size=".9rem" className="mr-1"/>Location:</p>
              <p>{ location }</p>
            </div>
            ) : null
          }
          <div>
            <p className="font-weight-bold">Original size: </p>
            <p>{ data.width } x { data.height }</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Photo;
