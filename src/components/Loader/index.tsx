import * as React from 'react';

import './Loader.scss';

type Props = {};

const Loader: React.SFC = () => {
  return (
    <div className="lds-roller">
      <div/>
      <div/>
      <div/>
      <div/>
      <div/>
      <div/>
      <div/>
      <div/>
    </div>
  );
};

export default Loader;
