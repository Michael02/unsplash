import * as React from 'react';
import { connect } from 'react-redux';
import { Search } from 'react-bootstrap-icons';

import ApiService from '../../services/api.service';
import {
  getPhrase,
  updatePhrase,
  searchingPhotos,
  searchingError,
  searchingResults,
} from '../../store/searchPhotos';

import './SearchBar.scss';

const select = (state: any) => ({
  phrase: getPhrase(state),
});
const mapActions = {
  updatePhrase,
  searchingPhotos,
  searchingError,
  searchingResults,
};

type MappedProps = ReturnType<typeof select>;
type MappedActions = typeof mapActions;

type Props = {
  history: any,
} & MappedActions & MappedProps;
type State = {};

class SearchBar extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.props.updatePhrase(e.target.value);
  }

  handleKeyPress = (e: React.KeyboardEvent) => {
    const { phrase, history } = this.props;
    if(phrase && e.keyCode === 13) {
      this.props.searchingPhotos();
      ApiService.searchPhotos(phrase)
        .then((res) => {
          this.props.searchingResults(res);
          history.push('/photos');
        })
        .catch((error) => {
          this.props.searchingError(error.message);
        });
    }
  }

  render() {
    const { phrase } = this.props;
    return (
     <div className="row search-bar">
       <div className="col-12 form-group w-100 mb-0">
        <input
          className="form-control search-bar-input"
          type="text"
          value={ phrase }
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => this.handleTextChange(e)}
          onKeyDown={(e: React.KeyboardEvent) => this.handleKeyPress(e)}
          placeholder="Search high-resolution photos"
        />
        <Search color="#000" size="1rem" className="search-icon"/>
       </div>
     </div>
    );
  }
}

export default connect<MappedProps, MappedActions>(select, mapActions)(SearchBar);
