import * as React from 'react';

import './PhotoContainer.scss';

type Props = {
  data: any,
  onClick: Function,
};

const Photo: React.SFC<Props> = (props) => {
  const {
    data,
    onClick,
  } = props;
  return (
    <div className="row d-flex justify-content-center photo-container mb-2">
      <div
        className="col-11 img-box py-3 d-flex justify-content-center pointer"
        onClick={() => onClick(data)}
      >
        <img src={data.urls.small}/>
      </div>
    </div>
  );
};

export default Photo;
